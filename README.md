<p align='center'><img src='/src/media/images/p-logo2.png' width='50px'></p>
<h1 align='center'> Brealy Fabian Padron Rodriguez Portfolio - (https://brealy-padron-portfolio-react.vercel.app/) </h1>

**Built with:**

![React](https://img.icons8.com/plasticine/48/000000/react.png)
![Typscript](https://img.icons8.com/color/48/000000/typescript.png)
![css3](https://img.icons8.com/color/48/000000/css3.png)


## Description

<p align='center'>My personal Portfolio Web App built with React using the Model-View-View-Model (MVVM) architectural pattern as the core logic for the Structure of the view and UI components using Typescript, styled-components and CSS for styles. 
  I'm also Using JavaScript Async/Await Fetch to connect API To retrieve My Github repositories.</p>
  Application is also Dockerized for development and production deployment.
  
## Front-end deployed in: Vercel

## Commands
  <p align='center'>  If you want to deploy it for test you'll need Node.js or Docker installed to run this project</p>

<h3 align='center'>You can clone this repository with the next command: </h3>
<p align='center'> <strong>git clone https://github.com/NigarumOvum/brealy-react-app </strong> </p>

<h3 align='center'>Go inside directory:</h3>
  <p align='center'> <strong> cd brealy-react-app </strong> </p>

<p align='center'>  then you can run inside the directory the command: </p>
  <p align='center'<strong>npm start </strong> </p>
  
  <p align='center'> Now you can acces the project thought this localhost url in port 3000  </p>
  <p align='center'<strong>http://localhost:3000</strong> </p>

<h2 align='center'>  also you can use Docker  </h2>

<p align='center'<strong>docker build --tag react-portfolio . </strong> </p>

<p align='center'<strong>docker run --publish 3000:3000 react-portfolio </strong> </p>

<h2 align='center'<strong>or  Docker Compose</strong> </h2>

<p align='center'<strong>docker-compose build  </strong> </p>

<p align='center'<strong>docker-compose run react-portfolio </strong> </p>

# About me:

- 🤔 I’m looking for more oportunities, projects and startups to growth my skills and achieve more and more experience in many different technologies. ![developer](https://img.icons8.com/external-flat-juicy-fish/24/000000/external-developer-devops-flat-flat-juicy-fish-2.png)
- ⚡ Fun facts: I really love play video games ![videog](https://img.icons8.com/color/24/000000/controller.png), play the guitar/sing ![guitar](https://img.icons8.com/external-vitaliy-gorbachev-flat-vitaly-gorbachev/24/000000/external-guitar-camping-vitaliy-gorbachev-flat-vitaly-gorbachev.png), learn about science ![science](https://img.icons8.com/cute-clipart/24/000000/biotech.png), go to the beach ![beach](https://img.icons8.com/fluency/24/000000/beach.png), learn new technological trends very often.
- I love the coffee! ![cofee](https://img.icons8.com/external-flat-juicy-fish/24/000000/external-developer-web-developer-flat-flat-juicy-fish-2.png)
-  Portfolio Website:<a href="https://brealy-padron-portfolio-react.vercel.app//"> Portfolio </a> 
- ![gh](https://img.icons8.com/cute-clipart/24/000000/github.png) Profile: <a href="https://github.com/nigarumovum/nigarumovum"> Brealy Padron GH Profile </a>
- ![spotify](https://img.icons8.com/fluency/24/000000/spotify.png) Profile: <a href="https://open.spotify.com/user/r8o2g959rb1dyp8fexucl2mbr"> Brealy Padron Spotify Profile </a>

:mailbox: Reach me out!

- neighbordevcr@gmail.com



[<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />](https://www.linkedin.com/in/bfpr131095/)

# Stats: 
![visitors](https://visitor-badge.glitch.me/badge?page_id=nigarumovum.nigarumovum)
