const Projects = [
  {
    icon: '',
    title: 'NeighborDev CR',
    description: 'Digital Marketing & Web Development self-company',
    ghLink: 'https://github.com/NigarumOvum/Neighbordev-CR',
    brLink: 'https://neighbordevcr.com',
    tecnologias: [
      {
        name: 'HTML5',
        imageUrl: 'https://img.icons8.com/color/96/000000/html-5--v1.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },
      {
        name: 'Sass',
        imageUrl: 'https://img.icons8.com/ios-filled/50/000000/sass.png',
      },
      {
        name: 'Javascript',
        imageUrl: 'https://img.icons8.com/color/96/000000/javascript--v1.png',
      },
      {
        name: 'jQuery',
        imageUrl: 'https://img.icons8.com/ios-filled/50/000000/jquery.png',
      },
      {
        name: 'Vue',
        imageUrl: 'https://img.icons8.com/color/96/000000/vue-js.png',
      },
      {
        name: 'MongoDB',
        imageUrl: 'https://img.icons8.com/color/48/000000/mongodb.png',
      }
    ],
  },
  {
    icon: `https://img.icons8.com/fluent-systems-regular/1000/ffffff/shop.png`,
    title: 'Tecnoelectro Comercio Online',
    description: 'Django Shop for a customer.',
    ghLink: 'https://github.com/NigarumOvum/Django-Tecnoelectro-Website',
    brLink: 'https://tecnoelectrocomercioonline.com',
    tecnologias: [
      {
        name: 'HTML5',
        imageUrl: 'https://img.icons8.com/color/96/000000/html-5--v1.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },
      {
        name: 'Javascript',
        imageUrl: 'https://img.icons8.com/color/96/000000/javascript--v1.png',
      },
      {
        name: 'jQuery',
        imageUrl: 'https://img.icons8.com/ios-filled/50/000000/jquery.png',
      },
      {
        name: 'Django',
        imageUrl: 'https://img.icons8.com/color/96/000000/django.png',
      },
      {
        name: 'Docker',
        imageUrl: 'https://img.icons8.com/color/48/000000/docker.png',
      }
    ],
  },
  {
    icon: '',
    title: 'It is a Skin Thing',
    description: 'Skin Store made in React.',
    ghLink: 'https://github.com/NigarumOvum/Its-Skin-Thing-CR-React',
    brLink: 'https://its-skin-thing-cr-react.vercel.app/',
    tecnologias: [

      {
        name: 'Javascript',
        imageUrl: 'https://img.icons8.com/color/96/000000/javascript--v1.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },
      {
        name: 'Sass',
        imageUrl: 'https://img.icons8.com/ios-filled/50/000000/sass.png',
      },
      {
        name: 'React',
        imageUrl: 'https://img.icons8.com/plasticine/100/000000/react.png',
      },
      {
        name: 'MongoDB',
        imageUrl: 'https://img.icons8.com/color/48/000000/mongodb.png',
      }
    ],
  },
  {
    icon: '',
    title: 'RadiOn - Online Radio',
    description: 'Online Radio Web App built in React',
    ghLink: 'https://github.com/NigarumOvum/radion-react',
    brLink: 'https://radion-react.vercel.app/',
    tecnologias: [

      {
        name: 'Javascript',
        imageUrl: 'https://img.icons8.com/color/96/000000/javascript--v1.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },
      {
        name: 'Sass',
        imageUrl: 'https://img.icons8.com/ios-filled/50/000000/sass.png',
      },
      {
        name: 'React',
        imageUrl: 'https://img.icons8.com/plasticine/100/000000/react.png',
      }
    ],
  },
  {
    icon: '',
    title: 'Tarotly',
    description: 'Tarot WebApp',
    ghLink: 'https://github.com/NigarumOvum/Tarotly-React',
    brLink: 'https://tarotly-react.vercel.app/',
    tecnologias: [

      {
        name: 'Javascript',
        imageUrl: 'https://img.icons8.com/color/96/000000/javascript--v1.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },
      {
        name: 'React',
        imageUrl: 'https://img.icons8.com/plasticine/100/000000/react.png',
      }
    ],
  },
  {
    icon: '',
    title: 'Costa Rica Travel',
    description: 'Travel Web App to show beutiful places in Costa Rica built in React',
    ghLink: 'https://github.com/NigarumOvum/Travel-CostaRica-React',
    brLink: 'https://travel-costa-rica-react.vercel.app/',
    tecnologias: [

      {
        name: 'TypeScript',
        imageUrl: 'https://img.icons8.com/color/48/000000/typescript.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },

      {
        name: 'React',
        imageUrl: 'https://img.icons8.com/plasticine/100/000000/react.png',
      }
    ],
  },
  {
    icon: '',
    title: 'MacOS Web App',
    description: 'MacOS Web App built in React',
    ghLink: 'https://github.com/NigarumOvum/MacOS-DesktopApp-React',
    brLink: 'https://mac-os-desktop-app-react.vercel.app/',
    tecnologias: [

      {
        name: 'TypeScript',
        imageUrl: 'https://img.icons8.com/color/48/000000/typescript.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },

      {
        name: 'React',
        imageUrl: 'https://img.icons8.com/plasticine/100/000000/react.png',
      }
    ],
  },
  {
    icon: '',
    title: 'Web Apps & Games',
    description: 'Fun Repo collection of apps made in Python(Brython)/JavaScript & some HTML5 games.',
    ghLink: 'https://github.com/NigarumOvum/Web-Apps-Games',
    brLink: 'https://nigarumovum.github.io/Web-Apps-Games/',
    tecnologias: [
    {
      name: 'HTML5',
      imageUrl: 'https://img.icons8.com/color/96/000000/html-5--v1.png',
    },
    {
      name: 'CSS',
      imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
    },
    {
      name: 'Javascript',
      imageUrl: 'https://img.icons8.com/color/96/000000/javascript--v1.png',
    },
    {
      name: 'jQuery',
      imageUrl: 'https://img.icons8.com/ios-filled/50/000000/jquery.png',
    }
  ],
},
  {
    icon: '',
    title: 'Brealy Padron Portfolio',
    description: 'Personal Portfolio React & Typescript',
    ghLink: 'https://github.com/NigarumOvum/brealy-react-app',
    brLink: 'https://brealy-padron-portfolio-react.vercel.app/',
    tecnologias: [

      {
        name: 'Typescript',
        imageUrl: 'https://img.icons8.com/color/48/000000/typescript.png',
      },
      {
        name: 'CSS',
        imageUrl: 'https://img.icons8.com/color/96/000000/css3.png',
      },
      {
        name: 'React',
        imageUrl: 'https://img.icons8.com/plasticine/100/000000/react.png',
      }
    ],
  }
]

export default Projects;
